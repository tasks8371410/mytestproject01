<?php

require __DIR__ . '/../app/Images.php';

use PHPUnit\Framework\TestCase;

final class ImagesTest extends TestCase {
    public function testImagesListLastElement() {
        $images = new Images('assets');
        $imagesList = $images->list();
        $result = end($imagesList);
        $this->assertEquals($result, 'assets/10.png');    
    }
}