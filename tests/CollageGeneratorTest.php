<?php

require __DIR__ . '/../app/CollageGenerator.php';

use PHPUnit\Framework\TestCase;

final class CollageGeneratorTest extends TestCase {
    public function testCollageHeight() {
        $generator = new CollageGenerator('assets', 'test1');
        $this->assertEquals($generator->collageHeight(), '1118');    
    }

    public function testCollageWidth() {
        $generator = new CollageGenerator('assets', 'test2');
        $this->assertEquals($generator->collageWidth(), '1870'); 
    }

    public function testCollageExist() {
        $generator = new CollageGenerator('assets', 'test3');
        $generator->collage();
        $type = mime_content_type('generate/test3.png');
        $result =  strstr($type, 'image/');
        $this->assertEquals($result, 'image/png');    
    }
}