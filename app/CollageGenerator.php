<?php
require_once 'vendor/autoload.php';
require __DIR__ . '/Images.php';
use Imagine\Image\Box;
use Imagine\Image\Point;
use Imagine\Gd\Imagine;
use Imagine\Image\Palette\RGB;

class CollageGenerator
{
    private string $directory = '';
    private int $imageMargin = 10;
    private int $imagewidth = 362;
    private int $imageHeight = 544;

    private string $collageTitle = '';

    private string $collageFormat = '.png';

    function __construct(string $imagesDirectory, string $collageTitle)
    {
        $this->directory = $imagesDirectory;
        $this->collageTitle = $collageTitle;
    }

    public function collage(): void
    {
        $imagine = new Imagine();
        $palette = new RGB();
        $color = $palette->color('#000', 0);
        $collage = $imagine->create(new Box($this->collageWidth(), $this->collageHeight()), $color);
        $imagesList = $this->images();
        $x = $this->imageMargin;
        $y = $this->imageMargin;

        foreach ($imagesList as $path) {
            $photo = $imagine->open($path);
            $collage->paste($photo, new Point($x, $y));
            $x += $this->imagewidth + $this->imageMargin;
            if ($x >= $this->collageWidth()) {
                $y += $this->imageHeight + $this->imageMargin;
                $x = $this->imageMargin;
            }
            if ($y >= $this->collageHeight()) {
                break;
            }
        }
        $collage->save('generate/' . $this->collageTitle . $this->collageFormat);
    }

    public function collageWidth(): int
    {
        $imagesInRow = 5;
        return $this->size($this->imagewidth, $imagesInRow);
    }
    public function collageHeight(): int
    {
        $imagesInColumn = 2;
        return $this->size($this->imageHeight, $imagesInColumn);
    }

    private function size(int $oneElemenSize, int $count): int
    {
        return ($oneElemenSize + $this->imageMargin) * $count + $this->imageMargin;
    }
    private function images()
    {
        $images = new Images($this->directory);
        return $images->list();
    }
}