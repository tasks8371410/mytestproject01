<?php

class Images
{
    private array $imagesList = [];
    private string $directory = '';

    function __construct(string $imagesDirectory)
    {
        $this->directory = $imagesDirectory;
        $this->include();
    }

    public function list()
    {
        natsort($this->imagesList);
        return $this->imagesList;
    }

    private function include(): void
    {
        $data = scandir(__DIR__ . '/../' . $this->directory);
        if (is_array($data)) {
            $this->fetchImages($data);
        }
        if (is_string($data)) {
            $this->imageData($data);
        }
    }

    private function fetchImages(array $images): void
    {
        foreach ($images as $image) {
            $this->imageData($image);
        }
    }

    private function imageData(string $image): void
    {
        if ($this->validateImage($image)) {
            $this->includeImage($image);
        }
    }

    private function validateImage(string $image): bool
    {
        $type = mime_content_type($this->imagePath($image));
        return strstr($type, 'image/');
    }

    private function includeImage(string $image): void
    {
        $this->imagesList[] = $this->imagePath($image);
    }

    private function imagePath(string $image): string
    {
        return $this->directory . '/' . $image;
    }
}